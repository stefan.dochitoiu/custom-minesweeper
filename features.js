const SIZE = 5;
const minesNumberFactor = 1 / 6;
const minesToFind = Math.ceil(minesNumberFactor * SIZE * SIZE);
const minesNumbersColors = ['blue', 'green', 'red', 'brown', 'blueviolet', 'purple', 'saddlebrown', 'black'];
var buttons, markedMines = 0, exploredCellsCounter = 0;

function safeCellsOnClick(row, col) {
  let cellsToExplore = [[row, col]], clickedValue = buttons[row][col].value;
  while (cellsToExplore.length) {
    let cell = cellsToExplore[0], button = buttons[cell[0]][cell[1]], cellValue = button.value,
    neighbors = [[cell[0] - 1, cell[1] - 1], [cell[0] - 1, cell[1]], [cell[0] - 1, cell[1] + 1], [cell[0], cell[1] + 1], [cell[0] + 1, cell[1] + 1], [cell[0] + 1, cell[1]], [cell[0] + 1, cell[1] - 1], [cell[0], cell[1] - 1]];
    for (const neighbor of neighbors) {
      if (neighbor[0] >= 0 && neighbor[0] < SIZE && neighbor[1] >= 0 && neighbor[1] < SIZE && cellValue === clickedValue) {
        cellsToExplore.push(neighbor);
      }
    }
    cellsToExplore.shift(cell);
    if (cellValue !== 'mined' && cellValue.substring(cellValue.length - 4) !== 'done') {
      button.style.backgroundImage = "url('')";
      if (cellValue === 'mines0') {
        button.setAttribute('class', 'mines0');   
      } else {
        button.style.color = minesNumbersColors[Number.parseInt(cellValue.substring(5)) - 1];
      }
      ++exploredCellsCounter;
      button.value += 'done';
    }
  }
  if (exploredCellsCounter === SIZE * SIZE - minesToFind) {
    outcome ("You won!!! ");
  }
}

function outcome(message) {
  for (const buttonLine of buttons) {
    for (const button of buttonLine) {
      button.setAttribute('onmousedown', '');
      button.style.backgroundImage = "url('')";
      if (button.value === 'mined') {
        button.style.backgroundImage = "url('mine.ico')";
      } else if (button.value.substring(0, 6) !== 'mines0') {
        button.style.color = minesNumbersColors[Number.parseInt(button.value.substring(5)) - 1];
      } else {
        button.setAttribute('class', 'mines0');   
      }
    }
  }
  document.getElementById('outcome').innerHTML = message;
  document.getElementById('outcome').innerHTML += '<button onClick="newGame();"> NEW GAME </button>';
  markedMines = 0;
  exploredCellsCounter = 0;
}

function getCellCoordinates(id) {
  let xPos = id.search('x');
  return [Number.parseInt(id.substring(3, xPos)), Number.parseInt(id.substring(xPos + 1))];
}

function handleClicks(event, id) {
  event.preventDefault();
  [row, col] = getCellCoordinates(id);
  let button = buttons[row][col], value = button.value;
  if (value.substring(value.length - 4) === 'done' || (event.button === 0 && button.name === 'marked')) {
    return;
  }
  if (event.button === 0 && value === 'mined') {
    outcome('You lose the game <br> Reason: you steped into a mined cell ');
    button.style.backgroundImage = "url('')";
    button.setAttribute('class', 'crash');
  } else if (event.button === 0) {
    safeCellsOnClick(row, col);
  } else if (event.button === 2 && button.name !== 'marked') {
    button.style.backgroundImage = "url('flag.ico')";
    button.setAttribute('name', 'marked');
    document.getElementById("markedMines").innerHTML = ++markedMines;
    document.getElementById("minesPlanedToFind").innerHTML = (minesToFind - markedMines);
  } else if (event.button === 2) {
    button.style.backgroundImage = "url('')";
    button.setAttribute('name', '');
    button.style.backgroundImage = "url('tile.ico')";
    document.getElementById("markedMines").innerHTML = --markedMines;
    document.getElementById("minesPlanedToFind").innerHTML = (minesToFind - markedMines);
  }
}

function newGame() {
  document.getElementById("outcome").innerHTML = '';
  document.getElementById("buttons").innerHTML = '';
  for (let i = 0; i < SIZE; ++i) {
    for (let j = 0; j < SIZE; ++j) {
      document.getElementById("buttons").innerHTML += '<button id="pos' + i + 'x' + j + '" onmousedown="plantMines(event, id);"> 0 </button>';
    }
    document.getElementById("buttons").innerHTML += '<br>';
  }
  buttons = Array(SIZE);
  for (let i = 0; i < SIZE; ++i) {
    buttons[i] = Array(SIZE);
    for (let j = 0; j < SIZE; ++j) {
      buttons[i][j] = document.getElementById("pos" + i + 'x' + j);
    }
  }
  document.getElementById("legend").innerHTML = 'Click anywhere in a cell!';
}

function plantMines(event, id) {
  if (event.button) {
    return;
  }
  let plantMine = function() {
    let row = Math.trunc(Math.random() * SIZE);
    let col = Math.trunc(Math.random() * SIZE);
    while (buttons[row][col].value) {
      ++col;
      if (col === SIZE) {
        col = 0;
        ++row;
        row = row === SIZE ? 0 : row;
      }
    }
    let button = buttons[row][col];
    button.setAttribute('value', 'mined');
    button.style.color = 'transparent';
    button.innerHTML = '9';
  }
  let firstZerosCoordinates = getCellCoordinates(id);
  --firstZerosCoordinates[0];
  --firstZerosCoordinates[1];
  for (let i = firstZerosCoordinates[0]; i < firstZerosCoordinates[0] + 3; ++i) {
    for (let j = firstZerosCoordinates[1]; j < firstZerosCoordinates[1] + 3; ++j) {
      if (i >= 0 && i < SIZE && j >= 0 && j < SIZE) {
        buttons[i][j].setAttribute('value', 'firstZeros');
      }
    }
  }
  document.getElementById('legend').innerHTML = '';
  for (let i = 0; i < minesToFind; ++i) {
    plantMine();
  }
  for (let row = 0; row < SIZE; ++row) {
    for (let col = 0; col < SIZE; ++col) {
      buttons[row][col].setAttribute('onmousedown', 'handleClicks(event, id);');
      buttons[row][col].setAttribute('oncontextmenu', 'return false;');
      let firstNeighborsRow = row - 1, firstNeighborsCol = col - 1, minesNr = 0;
      for (let i = firstNeighborsRow; i < firstNeighborsRow + 3; ++i) {
        for (let j = firstNeighborsCol; j < firstNeighborsCol + 3; ++j) {
          if (i >= 0 && i < SIZE && j >= 0 && j < SIZE && buttons[i][j].value === 'mined') {
            ++minesNr;
          }       
        }
      }
      if (buttons[row][col].value !== 'mined') {
        buttons[row][col].innerHTML  = '' + minesNr;;
        buttons[row][col].setAttribute('value', 'mines' + minesNr);
      }
    }
  }
  safeCellsOnClick(firstZerosCoordinates[0] + 1, firstZerosCoordinates[1] + 1);
  document.getElementById('outcome').innerHTML = 'You marked <label id="markedMines">' + markedMines + '</label>\
  mines so you would have to discover more <label id="minesPlanedToFind">' + (minesToFind - markedMines) + '</label> mines';
}